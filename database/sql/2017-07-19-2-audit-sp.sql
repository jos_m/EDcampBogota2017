-- Función que registrará la auditoría
CREATE OR REPLACE FUNCTION audit_data()
  RETURNS TRIGGER AS
  $BODY$
    BEGIN
      IF TG_OP = 'UPDATE' THEN
        INSERT INTO audits (audit_action, audit_table, previous_data, actual_data)
          VALUES (TG_OP, TG_TABLE_NAME, row_to_json(OLD.*), row_to_json(NEW.*));
        RETURN NEW;
      ELSEIF TG_OP = 'DELETE' THEN
        INSERT INTO audits (audit_action, audit_table, previous_data)
          VALUES (TG_OP, TG_TABLE_NAME, row_to_json(OLD.*));
        RETURN OLD;
      END IF;
      RETURN NEW;
    END;
  $BODY$
  LANGUAGE plpgsql;

-- Trigger que ejecutará la función de auditoría
-- en la tabla de facturación
CREATE TRIGGER audit_data_invoices
  AFTER UPDATE OR DELETE ON invoices
  FOR EACH ROW EXECUTE PROCEDURE audit_data();

-- Trigger que ejecutará la función de auditoría
-- en la tabla de suscripciones
CREATE TRIGGER audit_data_subscriptions
  AFTER UPDATE OR DELETE ON subscriptions
  FOR EACH ROW EXECUTE PROCEDURE audit_data();

-- Trigger que ejecutará la función de auditoría
-- en la tabla de precios
CREATE TRIGGER audit_data_prices
  AFTER UPDATE OR DELETE ON prices
  FOR EACH ROW EXECUTE PROCEDURE audit_data();
